import Vue from 'vue'
import VueRouter from 'vue-router'
import Coordinador from '../views/Coordinador.vue'
import Administrador from '../views/Administrador.vue'
import Login from '../views/Login.vue'

import Tabla from '../components/Tabla.vue'
import Categoria from '../components/Categoria.vue'

import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
    meta: {
      titulo: 'Login'
    }
  },
  {
    path: '/Coordinador',
    name: 'Coordinador',
    component: Coordinador,
    meta: {
      rutaProtegida: true,
      titulo: 'Coordinador'
    }
  },
  {
    path: '/Administrador',
    name: 'Administrador',
    component: Administrador,
    children: [
      {
        path: 'atletas',
        component: Tabla,
        meta: { titulo: 'Administrador - Atletas' }
      },
      {
        path: 'categorias',
        component: Categoria,
        meta: { titulo: 'Administrador - Categorias' }
      }
    ],
    meta: {
      rutaProtegida: true,
      titulo: 'Administrador'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const RutaEsProtegida = to.matched.some(item => item.meta.rutaProtegida)
  document.title = to.meta.titulo
  if (RutaEsProtegida && store.state.usuario === null) {
    next('/')
  } else {
    next()
  }
})

export default router
