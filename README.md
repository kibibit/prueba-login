# prueba-login

Proyecto Hecho con Vue y Bootstrap.

Use Vuex para el manejo de la sesion y roles.

Este proyecto consiste en una pagina para hacer login, en la cual hay dos roles
- Coordinador
- Administrador
Al no tener backend y bases de datos los usuarios estan guardados en una variable.

Para acceder como Coordinador, el usuario y contraseña es:
    usuario: juan@abc.com    contraseña: juanabc

Para acceder como Administrador, el usuario y contraseña es:
    usuario: diego@abc.com    contraseña: diegoabc

Para probar el proyecto solo escribe en una terminal
```
npm install
npm run serve
```
Para compilar el proyecto solo escribe en una terminal
```
npm install
npm run build
```

puedes ver el proyecto funcionando [aqui](https://kibibit.gitlab.io/prueba-login).
